// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { GenreType } from "core/types/genre.type";

type Data = {
  resultCount: number,
  results: GenreType[] | null,
}

const results = [
  {
    id: 1330,
    name: "Blues",
  },
  {
    id: 1331,
    name: "Classical",
  },
  {
    id: 1332,
    name: "Country",
  },
  {
    id: 1333,
    name: "Dance",
  },
  {
    id: 1334,
    name: "Electronic",
  },
  {
    id: 1335,
    name: "Hip-Hop",
  },
  {
    id: 1336,
    name: "Jazz",
  },
  {
    id: 1337,
    name: "R&B / Soul",
  },
  {
    id: 1338,
    name: "Reggae / Dancehall",
  },
  {
    id: 1440,
    name: "Alternative",
  },
  {
    id: 1441,
    name: "Instrumental",
  },
  {
    id: 1442,
    name: "Pop",
  },
  {
    id: 1443,
    name: "Soundtrack",
  },
  {
    id: 1444,
    name: "Rock",
  },
  {
    id: 1445,
    name: "Techno",
  }
]

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json({
    resultCount: results.length,
    results,
  })
}
