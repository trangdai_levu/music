import { NextPage } from "next";
import { END } from "redux-saga"
import { wrapper, StateType, SagaStore } from "store";
import Layout from "modules/components/layouts";
import { fetchGenre, fetchMusic } from "modules/slice";
import Home from "modules/components/Home";

const Index: NextPage = () => {
  return (
    <Layout>
      <Home />
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(async ({ store }) => {

  const state: StateType = store.getState();
  if (!state.genre?.data) {
    store.dispatch(fetchGenre());
  }
  if (!state.music?.data) {
    store.dispatch(fetchMusic({ term: "cafe" }));
  }
  store.dispatch(END);

  await (store as SagaStore).sagaTask.toPromise();
})

export default Index;
