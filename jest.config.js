const { pathsToModuleNameMapper } = require("ts-jest/utils")
const { compilerOptions: { paths: tsconfigPaths } } = require("./tsconfig");

module.exports = {
  roots: ["<rootDir>"],
  modulePaths: ["<rootDir>"],
  testEnvironment: "jsdom",
  collectCoverageFrom: [
    "**/*.{js,jsx,ts,tsx}",
    "!**/*.d.ts",
    "!**/node_modules/**",
  ],
  setupFilesAfterEnv: ["<rootDir>/tests/jest.setup.js"],
  testPathIgnorePatterns: ["/node_modules/", "/.next/", "/cypress/", "/_docs/"],
  testMatch: ["**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)"],
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
    "^.+\\.css$": "<rootDir>/tests/css-transform.js",
  },
  transformIgnorePatterns: [
    "/node_modules/",
    "^.+\\.module\\.(css|sass|scss)$",
  ],
  moduleNameMapper: {
    "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy",
    "tests/(.*)": "<rootDir>/tests/$1",
    ...pathsToModuleNameMapper(tsconfigPaths)
  },
}