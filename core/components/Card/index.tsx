import clsx from "clsx";
import Image from "next/image";
import { BaseCmpType } from "core/types/ui.type";
import styles from "./styles/Card.module.scss";

type Props = BaseCmpType & {
  className?: string,
  title: string,
  description?: string,
  imageUrl?: string,
  onClick?: React.MouseEventHandler<HTMLElement>,
};

export default function Card({
  className,
  title,
  description,
  imageUrl,
  onClick,
  ...restProps
}: Props) {

  return (
    <div
      className={clsx(styles.card, className)}
      onClick={onClick}
      {...restProps}
    >
      <div className={styles.imgwrap}>
        <Image
          src={imageUrl || "/images/no-image.jpg"}
          alt={`${title} ${description}`}
          width={112}
          height={112}
          layout="responsive"
        />
      </div>
      <p className={clsx("txt__truncate", styles.title)}>
        {title}
      </p>
      <p className={styles.description}>
        {description}
      </p>
    </div>
  );
}
