import * as React from "react";
import clsx from "clsx";
import styles from "./styles/SvgIcon.module.css";

type Props = {
  children: React.ReactNode;
  className?: string;
  width?: string;
  height?: string;
  titleAccess?: string;
  viewBox?: string;
  fontSize?: number | string;
  color?: string;
  style?: React.CSSProperties;
}

function SvgIcon(props: Props, ref: React.ForwardedRef<SVGSVGElement>) {
  const {
    children,
    style,
    className,
    titleAccess,
    width,
    height,
    fontSize = 24,
    color = "inherit",
    viewBox = "0 0 24 24",
    ...rest
  } = props;

  return (
    <svg
      style={style}
      className={clsx(styles.svgIcon, className)}
      focusable="false"
      viewBox={viewBox}
      width={width}
      height={height}
      preserveAspectRatio="xMidYMid meet"
      color={color}
      fontSize={fontSize}
      aria-hidden={titleAccess ? "false" : "true"}
      role={titleAccess ? "img" : "presentation"}
      ref={ref}
      {...rest}
    >
      {children}
      {titleAccess ? <title>{titleAccess}</title> : null}
    </svg>
  );
};

type SvgIconProps = Omit<Props, "children">
export type { SvgIconProps };
export default React.forwardRef(SvgIcon);

/**
 * How to use
 *
import SvgIcon from "components/SvgIcon";
...
  <SvgIcon
    viewBox="0 0 15 18"
    fontSize="18px"
    color="cyan"
    {...props}
  >
    <path d="M5.5,...  Z"></path>
  </SvgIcon>
...
*/
