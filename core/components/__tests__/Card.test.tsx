import { describe, expect } from "@jest/globals";
import { mount } from "enzyme";
import mountTest from "tests/shared/mountTest";
import Card from "../Card";

describe("Components ◆ Card", () => {
  mountTest(<Card title="125"/>);

  it("should render", () => {
    const wrapper = mount(<Card title="125" />);
    expect(wrapper.find(".title").text()).toBe("125");
    expect(wrapper.find(".description").text()).toBe("");
    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });

  it("should trigger onClick", () => {
    const onClick = jest.fn();
    const wrapper = mount(<Card title="abc" onClick={onClick} />);
    wrapper.simulate("click");
    expect(onClick).toHaveBeenCalledWith(
      expect.objectContaining({
        type: "click",
        preventDefault: expect.any(Function),
      }),
    );
  });
});
