import { describe, expect } from "@jest/globals";
import { mount } from "enzyme";
import mountTest from "tests/shared/mountTest";
import Tag from "../Tag";

describe("Components ◆ Tag", () => {
  mountTest(<Tag name="tagname" />);

  it("should render", () => {
    const wrapper = mount(<Tag name="tagname" />);
    expect(wrapper.text()).toBe("tagname");
    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });

  it("should render with custom class", () => {
    const wrapper = mount(<Tag name="tagname" className="selected"/>);
    expect(wrapper.getDOMNode().className).toContain("selected");
    wrapper.unmount();
  });

  it("should trigger onClick", () => {
    const onClick = jest.fn();
    const wrapper = mount(<Tag name="abc" onClick={onClick} />);
    wrapper.simulate("click");
    expect(onClick).toHaveBeenCalledWith(
      expect.objectContaining({
        type: "click",
        preventDefault: expect.any(Function),
      }),
    );
  });

});
