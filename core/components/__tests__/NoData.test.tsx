import { describe, expect } from "@jest/globals";
import { mount } from "enzyme";
import mountTest from "tests/shared/mountTest";
import NoData from "../NoData";

describe("Components ◆ NoData", () => {
  mountTest(<NoData />);

  it("should render", () => {
    const wrapper = mount(<NoData />);
    expect(wrapper.find("svg").prop("width")).toBe("64");
    expect(wrapper.find("svg").prop("height")).toBe("41");
    expect(wrapper.text()).toBe("No Data");
    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });

  it("should render with custom class", () => {
    const wrapper = mount(<NoData className="customNoData"/>);
    expect(wrapper.getDOMNode().className).toContain("customNoData");
    wrapper.unmount();
  });

  it("should render with custom style", () => {
    const wrapper = mount(<NoData style={{ color: "red", fontSize: 32 }} />);
    expect(wrapper.text()).toBe("No Data");
    expect(wrapper.prop("style").color).toBe("red");
    wrapper.unmount();
  });


});
