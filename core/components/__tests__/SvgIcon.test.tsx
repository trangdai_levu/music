import { describe, expect } from "@jest/globals";
import { mount } from "enzyme";
import mountTest from "tests/shared/mountTest";
import SvgIcon from "../SvgIcon";

describe("Components ◆ SvgIcon", () => {
  mountTest(<SvgIcon><rect /></SvgIcon>);

  it("should render", () => {
    const wrapper = mount(<SvgIcon><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" /></SvgIcon>);
    expect(wrapper.getDOMNode().getAttribute("focusable")).toBe("false");
    expect(wrapper.getDOMNode().getAttribute("aria-hidden")).toBe("true");
    expect(wrapper.getDOMNode().getAttribute("viewBox")).toBe("0 0 24 24");
    expect(wrapper).toMatchSnapshot();
    wrapper.unmount();
  });

  it("should render with titleAccess", () => {
    const wrapper = mount(<SvgIcon titleAccess="atitle">
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>);
    expect(wrapper.find("title").text()).toContain("atitle");
    wrapper.unmount();
  });

  it("should render with custom style", () => {
    const wrapper = mount(<SvgIcon style={{ color: "red", fontSize: 32 }} >
      <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>);
    expect(wrapper.prop("style").color).toBe("red");
    expect(wrapper.prop("style").marginBottom).toBeUndefined();
    wrapper.unmount();
  });

});
