import clsx from "clsx";
import { BaseCmpType } from "core/types/ui.type";
import styles from "./styles/Tag.module.scss";

type Props = BaseCmpType & {
  className?: "selected" | string,
  name: string,
  onClick?: React.MouseEventHandler<HTMLElement>,
};

export default function Tag(props: Props) {
  const {
    className,
    name,
    onClick,
    ...restProps
  } = props;

  return (
    <div
      className={clsx(styles.tag, className)}
      onClick={onClick}
      {...restProps}
    >
      {name}
    </div>
  );
}
