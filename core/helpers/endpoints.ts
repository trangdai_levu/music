export const ENDPOINTS = {
  GET_GENRE: `${process.env.DB_HOST_GENRE}/api/genre`,
  SEARCH_MUSIC: `${process.env.DB_HOST}/search`,

  // sample
  // rewardsByUser: (userId, rewardId) => `${BASE_URL}/eg/users/${userId}/rewards/${rewardId}`,
};
