export type OptionModel = {
  name: string,
  value: any,
  disabled?: boolean,
};

export type BaseCmpType = {
  className?: string,
  style?: React.CSSProperties,
};

