export type ActionType = {
  type: string,
  payload?: any,
};

export type ResponseSuccessType = {
  resultCount: number;
  results: any[];
}

type QueryParametersType = {
  output?: string,
  callback?: string,
  country?: string,
  limit?: string,
  term?: string,
  lang?: string,
}

export type ResponseFailureType = {
  errorMessage: string,
  queryParameters?: QueryParametersType,
}

export type ListDataType<T> = {
  resultCount: number,
  data: T[] | null,
  isError: boolean,
  errorMessage?: string,
}
