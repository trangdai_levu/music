export type MusicType = {
  trackId: number | string,
  trackName: string,
  artistName?: string,
  artworkUrl100?: string,
}

export type RequestSearchMusicType = {
  term: string,
  attribute?: "genreIndex",
  limit?: number
}
