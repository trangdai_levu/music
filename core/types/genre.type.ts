export type GenreType = {
  id: number | string,
  name: string,
  selected?: boolean,
}
