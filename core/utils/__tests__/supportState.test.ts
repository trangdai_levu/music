import { describe, expect } from "@jest/globals";
import { getDomainName } from "../supportState";

describe("Utils ❖ supportState", () => {
  it("getDomainName should work", () => {
    const name = "music";
    const subName = "genre";
    expect(getDomainName(name, subName)).toBe("music/genre");
    expect(getDomainName(name, subName)).not.toBe("[music | genre]");
    expect(getDomainName(name, subName)).not.toBe("[Music/GENRE]");
  });
});