// @ts-nocheck
import { getSafeValue } from "../get";

describe("Utils ❖ getSafeValue", () => {
  it("should work", () => {
    var obj = {
      "book": {
        "name": "Harry Potter and the Goblet of Fire",
        "author": {
          name: "J. K. Rowling",
          age: 23,
        },
        "year": 2000,
        "genre": "Fantasy Fiction",
        "bestseller": true
      }
    }

    expect(getSafeValue(() => obj.a.lot.of.properties)).toEqual(undefined);
    expect(getSafeValue(() => obj.book.author.birth.day, "aaa")).toBe("aaa");
    expect(getSafeValue(() => obj.book.genre)).toBe("Fantasy Fiction");
  });
});
