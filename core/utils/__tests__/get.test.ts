import { describe, expect } from "@jest/globals";
import { getLabel, getAvailableArray } from "../get";

describe("Utils ❖ get", () => {
  it("getLabel should work with string-key", () => {
    const TEST_LABELS = {
      primaryGenreName: "Rock"
    };
    expect(getLabel(TEST_LABELS, "primaryGenreName")).toBe("Rock");
  });

  it("getLabel should work with number-key", () => {
    const TEST_STATUS = {
      4: "Dragons"
    };

    const status = getLabel(TEST_STATUS, 4);
    expect(status).toBe("Dragons");

    const statusWrong = getLabel(TEST_STATUS, 3);
    expect(statusWrong).toBe(undefined);
  });

  it("getAvailableArray should work", () => {
    expect(getAvailableArray(null)).toBe(null);
    expect(getAvailableArray("null")).toBe(null);
    expect(getAvailableArray(123)).toBe(null);
    expect(getAvailableArray(true)).toBe(null);
    expect(getAvailableArray(false)).toBe(null);
    expect(getAvailableArray({ "a" : 1 })).toBe(null);
    expect(getAvailableArray([])).toBe(null);
    expect(getAvailableArray(new Array())).toBe(null);
    // this is available-array
    expect(getAvailableArray([1, 2, 3])).toEqual([1, 2, 3]);
    expect(getAvailableArray([
      { 4: "Dragons", "name": "J. K. Rowling" },
      { age: 23 }
    ])).toEqual([
      { 4: "Dragons", "name": "J. K. Rowling" },
      { age: 23 }
    ]);
  });
});
