export function getLabel(labelObj: { [key in (string | number)]: any }, key: string | number) {
  return labelObj && labelObj[key];
}

export function getAvailableArray(array: any): ([] | null) {
  if ((!Array.isArray(array) || !array.length)) return null;
  return getSafeValue(() => array, null);
}

export function getSafeValue(fn: () => any, defaultValue?: any) {
  /**
   * copies ::: https://silvantroxler.ch/2017/avoid-cannot-read-property-of-undefined/
   * use it like this
        getSafeValue(() => obj.a.lot.of.properties);

   * or add an optional default value
        getSafeValue(() => obj.a.lot.of.properties, 'nothing');
   */
  try {
    return fn();
  } catch (e) {
    return defaultValue;
  }
}
