export type { StateType as GenreStateType } from "modules/slice/genre";
export {
  NAME as GENRE_SLICE,
  saga as genreSaga,
  reducer as genreReducer,
  // Action
  fetchGenre,
  updateStatusGenre,
  // Selectors
  selectGenres,
  selectNextSelectedGenres,
} from "modules/slice/genre";

export type { StateType as MusicStateType } from "modules/slice/genre";
export {
  NAME as MUSIC_SLICE,
  saga as musicSaga,
  reducer as musicReducer,
  // Action
  fetchMusic,
  updateSearchText,
  // Selectors
  selectMusics,
  selectResultCount,
  selectSearchText,
} from "modules/slice/music";

