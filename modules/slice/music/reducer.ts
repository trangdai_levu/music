import produce, { Draft } from "immer";
import { HYDRATE } from "next-redux-wrapper";
import { ActionType, ListDataType } from "core/types/api.type";
import { MusicType } from "core/types/music.type";
import { ACTION_TYPES, NAME } from "./actions";

export type StateType = ListDataType<MusicType> & {
  searchText?: string
};

const INITIAL_STATE = {
  isError: false,
  resultCount: 0,
  data: null,
  searchText: "",
}

const reducer = produce((draft: Draft<StateType>, action: ActionType) => {
  switch (action.type) {
  case HYDRATE: {
    return { ...draft, ...action.payload[NAME] }
  }
  case ACTION_TYPES.UPDATE_SEARCH_TEXT: {
    draft.searchText = action.payload;
    break;
  }
  case ACTION_TYPES.FETCH_SUCCESS: {
    const { resultCount, results } = action.payload;
    draft.isError = false;
    draft.resultCount = resultCount;
    draft.data = results.map((track: MusicType) => ({
      trackId: track.trackId,
      trackName: track.trackName,
      artistName: track.artistName || null,
      artworkUrl100: track.artworkUrl100 || null,
    }));
    break;
  }
  case ACTION_TYPES.FETCH_FAILURE: {
    const { errorMessage } = action.payload;
    draft.isError = true;
    draft.resultCount = 0;
    draft.errorMessage = errorMessage;
    break;
  }
  default:
    return draft;
  }
}, INITIAL_STATE)

export default reducer;
