export type { StateType } from "./reducer";
export { default as reducer } from "./reducer";
export { default as saga } from "./saga";
export { NAME, fetchMusic, updateSearchText } from "./actions";
export { selectMusics, selectResultCount, selectSearchText } from "./selectors";
