import { put, takeLatest } from "redux-saga/effects";
import queryString from "query-string";
import { ENDPOINTS } from "core/helpers/endpoints";
import { ResponseSuccessType, ActionType } from "core/types/api.type";
import { RequestSearchMusicType } from "core/types/music.type";
import {
  ACTION_TYPES,
  fetchMusicFailure,
  fetchMusicSuccess
} from "./actions";

export function* fetchDataMusic(action: ActionType) {

  const query = action.payload as RequestSearchMusicType;
  console.log(query);
  try {
    const url = queryString.stringifyUrl({
      url: ENDPOINTS.SEARCH_MUSIC,
      query: {
        ...query,
        ...!query.term.trim().length && { term: "cafe" },
        ...{ entity: "musicTrack" }
      }
    });

    const res: Response = yield fetch(url)
    const data: ResponseSuccessType = yield res.json();
    yield put(fetchMusicSuccess(data));
  } catch (err) {
    yield put(fetchMusicFailure(err));
  }
}

export default function* musicSaga() {
  yield takeLatest(ACTION_TYPES.FETCH_REQUEST, fetchDataMusic);
}
