import { describe, expect } from "@jest/globals";
import { MusicType } from "core/types/music.type";
import {
  selectMusics,
  selectResultCount,
  selectSearchText
} from "../selectors";
import { NAME, StateType } from "../";

describe("STATE - SELECTOR ♥ MUSIC", () => {
  const data: MusicType[] = [{ trackId: "369572962", trackName: "birthday cake" }];
  const state: StateType = {
    isError: false,
    resultCount: 1,
    data,
    searchText: "cake"
  };

  it("selectMusics　．selectResultCount　．selectSearchText", () => {
    const musicList = selectMusics({ [NAME]: state });
    expect(musicList).toEqual(state.data);
    expect(selectSearchText({ [NAME]: state })).toEqual("cake");
    expect(selectResultCount({ [NAME]: state })).toEqual(1);

  });
});
