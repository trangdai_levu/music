import { describe, expect } from "@jest/globals";
import reducer from "../reducer";
import { ACTION_TYPES } from "../actions";

describe("STATE - REDUCER ♥ MUSIC", () => {
  it("should handle fetch music SUCCESS", () => {
    const dummyMusics ={
      resultCount: 1,
      results: [{
        trackId: "369572962",
        trackName: "birthday cake",
      }],
    };

    expect(
      reducer(undefined, {
        type: ACTION_TYPES.FETCH_SUCCESS,
        payload: dummyMusics,
      })
    ).toEqual({
      isError: false,
      resultCount: 1,
      searchText: "",
      data: [{
        trackId: "369572962",
        trackName: "birthday cake",
        artistName: null,
        artworkUrl100: null
      }]
    });
  });

  it("should handle fetch music FAILURE", () => {
    expect(
      reducer(undefined, {
        type: ACTION_TYPES.FETCH_FAILURE,
        payload: { errorMessage: "Something went wrong" },
      })
    ).toEqual(expect.objectContaining({
      isError: true,
      errorMessage: "Something went wrong",
    }));
  });

  it("should UPDATE_SEARCH_TEXT music", () => {
    expect(
      reducer({
        isError: false,
        resultCount: 0,
        data: null,
        searchText: "",
      }, {
        type: ACTION_TYPES.UPDATE_SEARCH_TEXT,
        payload: "cake",
      })
    ).toEqual(expect.objectContaining({
      searchText: "cake",
    }));
  });
});
