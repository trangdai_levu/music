import { createSelector } from "reselect";
import { getAvailableArray } from "core/utils/get";
import { StateType } from "./reducer";
import { NAME as MUSIC_SLICE } from "./actions";

const selectStateMusics = (state: { [MUSIC_SLICE]: StateType}) => state[MUSIC_SLICE];

export const selectMusics = createSelector(
  [selectStateMusics],
  (state) => {
    if (
      !state
      || state.isError
      || !state.resultCount
    ) return null;
    return getAvailableArray(state.data);
  }
);

export const selectResultCount = createSelector(selectStateMusics, (state) => state.resultCount);

export const selectSearchText = createSelector(selectStateMusics, (state) => state.searchText);
