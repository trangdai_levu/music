import { getDomainName } from "core/utils/supportState";
import { ResponseSuccessType, ResponseFailureType } from "core/types/api.type";
import { RequestSearchMusicType } from "core/types/music.type";

export const NAME = "music";
const DOMAIN_NAME = getDomainName("music", NAME);

/**
 * Action Types
 */

export const ACTION_TYPES = {
  FETCH_REQUEST: `${DOMAIN_NAME}/FETCH_REQUEST`,
  FETCH_CANCELLED: `${DOMAIN_NAME}/FETCH_CANCELLED`,
  FETCH_SUCCESS: `${DOMAIN_NAME}/FETCH_SUCCESS`,
  FETCH_FAILURE: `${DOMAIN_NAME}/FETCH_FAILURE`,
  UPDATE_SEARCH_TEXT: `${DOMAIN_NAME}/UPDATE_SEARCH_TEXT`,
};

/**
 * Action Creators
 */

export function fetchMusic(payload?: RequestSearchMusicType) {
  return { type: ACTION_TYPES.FETCH_REQUEST, payload }
}

export function cancelFetchMusic() {
  return { type: ACTION_TYPES.FETCH_CANCELLED }
}

export function fetchMusicSuccess(payload: ResponseSuccessType) {
  return {
    type: ACTION_TYPES.FETCH_SUCCESS,
    payload,
  }
}

export function fetchMusicFailure(payload: ResponseFailureType) {
  return {
    type: ACTION_TYPES.FETCH_FAILURE,
    payload,
  }
}

export function updateSearchText(payload: string) {
  return {
    type: ACTION_TYPES.UPDATE_SEARCH_TEXT,
    payload
  }
}
