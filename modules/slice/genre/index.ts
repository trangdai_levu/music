export type { StateType } from "./reducer";
export { default as reducer } from "./reducer";
export { default as saga } from "./saga";
export { NAME, fetchGenre, updateStatusGenre } from "./actions";
export { selectGenres, selectNextSelectedGenres } from "./selectors";
