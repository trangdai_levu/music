import { put, takeEvery } from "redux-saga/effects";
import { ENDPOINTS } from "core/helpers/endpoints";
import { ResponseSuccessType } from "core/types/api.type";
import {
  ACTION_TYPES,
  fetchGenreFailure,
  fetchGenreSuccess
} from "./actions";

export function* fetchDataGenre() {
  try {
    const res: Response = yield fetch(ENDPOINTS.GET_GENRE);
    const data: ResponseSuccessType = yield res.json();
    yield put(fetchGenreSuccess(data));
  } catch (err) {
    yield put(fetchGenreFailure(err));
  }
}

export default function* genreSaga() {
  yield takeEvery(ACTION_TYPES.FETCH_REQUEST, fetchDataGenre);
}
