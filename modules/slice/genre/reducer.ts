import produce, { Draft } from "immer";
import { HYDRATE } from "next-redux-wrapper";
import { ActionType, ListDataType } from "core/types/api.type";
import { GenreType } from "core/types/genre.type";
import { ACTION_TYPES, NAME } from "./actions";

export type StateType = ListDataType<GenreType>;

const INITIAL_STATE = {
  isError: false,
  resultCount: 0,
  data: null as (GenreType[] | null),
}

const reducer = produce((draft: Draft<StateType>, action: ActionType) => {
  switch (action.type) {
  case HYDRATE: {
    return { ...draft, ...action.payload[NAME] }
  }
  case ACTION_TYPES.UPDATE_STATUS: {
    const data = draft.data as GenreType[];
    const index = data?.findIndex(genre => genre.id === action.payload.id);
    data[index] = action.payload;
    break;
  }
  case ACTION_TYPES.FETCH_SUCCESS: {
    const { resultCount, results } = action.payload;
    draft.isError = false;
    draft.resultCount = resultCount;
    draft.data = results;
    break;
  }
  case ACTION_TYPES.FETCH_FAILURE: {
    const { errorMessage } = action.payload;
    draft.isError = true;
    draft.errorMessage = errorMessage;
    break;
  }
  default:
    return draft;
  }
}, INITIAL_STATE)

export default reducer;
