import { createSelector } from "reselect";
import produce from "immer";
import { getAvailableArray } from "core/utils/get";
import { GenreType } from "core/types/genre.type";
import { StateType } from "./reducer";
import { NAME as GENRE_SLICE } from "./actions";

const selectStateGenres = (state: { [GENRE_SLICE]: StateType}) => state[GENRE_SLICE];

export const selectGenres = createSelector(
  [selectStateGenres],
  (stateGenres) => {
    if (
      !stateGenres
      || stateGenres.isError
      || !stateGenres.resultCount
    ) return null;
    return getAvailableArray(stateGenres.data);
  }
);

export const selectNextSelectedGenres = (genres: GenreType[], updatedGenre: GenreType) => {
  const nextGenres = produce(genres, draftState => {
    const index = draftState?.findIndex(genre => genre.id === updatedGenre.id);
    draftState[index] = updatedGenre;
  });
  return nextGenres?.filter((genre: GenreType) => genre.selected);
}
