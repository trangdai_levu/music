import { describe, expect } from "@jest/globals";
import { enableFetchMocks } from "jest-fetch-mock";
import { runSaga } from "redux-saga";
import { takeEvery } from "redux-saga/effects";
import { ACTION_TYPES, fetchGenreSuccess } from "../actions";

import genreSaga, { fetchDataGenre } from "../saga";

type ACTIONTYPE = typeof ACTION_TYPES;
enableFetchMocks();

describe("STATE - SAGA ♥ GENRE", () => {
  test("should load and set the image stats in case of success", async () => {
    const dispatchedActions: ACTIONTYPE[] = [];
    const dummyGenres = {
      resultCount: 1,
      results: [{ id: 1445, name: "Techno" }],
    };
    const requestGenres = jest.fn(() => Promise.resolve(dummyGenres));
    const genreS = genreSaga().next().value;
    expect(genreS).toEqual(takeEvery(ACTION_TYPES.FETCH_REQUEST, fetchDataGenre));

    // await runSaga({
    //   dispatch: (action: ACTIONTYPE)  => dispatchedActions.push(action),
    // }, fetchDataGenre);

    // expect(requestGenres).toHaveBeenCalledTimes(1);
    // expect(dispatchedActions).toEqual([fetchGenreSuccess(dummyGenres)]);

    requestGenres.mockClear();
  });
});


