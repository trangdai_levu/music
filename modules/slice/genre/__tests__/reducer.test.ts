import { describe, expect } from "@jest/globals";
import reducer from "../reducer";
import { ACTION_TYPES } from "../actions";

describe("STATE - REDUCER ♥ GENRE", () => {
  it("should handle fetch genre SUCCESS", () => {
    const dummyGenres = {
      resultCount: 1,
      results: [{ id: 1445, name: "Techno" }],
    };

    expect(
      reducer(undefined, {
        type: ACTION_TYPES.FETCH_SUCCESS,
        payload: dummyGenres,
      })
    ).toEqual({
      isError: false,
      resultCount: dummyGenres.resultCount,
      data: dummyGenres.results
    });
  });

  it("should handle fetch genre FAILURE", () => {
    expect(
      reducer(undefined, {
        type: ACTION_TYPES.FETCH_FAILURE,
        payload: { errorMessage: "Something went wrong" },
      })
    ).toEqual(expect.objectContaining({
      isError: true,
      errorMessage: "Something went wrong",
    }));
  });

  it("should UPDATE genre", () => {
    expect(
      reducer({
        isError: false,
        resultCount: 1,
        data: [{ id: 1445, name: "Techno" }, { id: 1447, name: "Imagine" }],
      }, {
        type: ACTION_TYPES.UPDATE_STATUS,
        payload: { id: 1445, name: "Techno", selected: true },
      })
    ).toEqual({
      isError: false,
      resultCount: 1,
      data: [{ id: 1445, name: "Techno", selected: true }, { id: 1447, name: "Imagine" }]
    });
  });
});
