import { describe, expect } from "@jest/globals";
import { GenreType } from "core/types/genre.type";
import {
  selectGenres,
  selectNextSelectedGenres,
} from "../selectors";
import { NAME, StateType } from "../";

describe("STATE - SELECTOR ♥ GENRE", () => {
  const data: GenreType[] = [{ id: 1445, name: "Techno" }];
  const state: StateType = {
    isError: false,
    resultCount: 1,
    data,
  };

  it("selectGenres >>> should return genres", () => {
    const genres = selectGenres({ [NAME]: state });
    expect(genres).toEqual(state.data);
  });

  it("selectNextSelectedGenres >>> should return correct genres", () => {
    const newGenre = { id: 1445, name: "Imagine", selected: true };
    const newGenre2 = { id: 1447, name: "Imagine", selected: true };
    const genres = selectNextSelectedGenres(data, newGenre);
    expect(genres).toEqual([newGenre]);
    expect(selectNextSelectedGenres(data, newGenre2)).not.toEqual([newGenre2]);
  });
});