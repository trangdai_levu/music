import { getDomainName } from "core/utils/supportState";
import { ResponseSuccessType, ResponseFailureType } from "core/types/api.type";
import { GenreType } from "core/types/genre.type";

export const NAME = "genre";
const DOMAIN_NAME = getDomainName("music", NAME);

/**
 * Action Types
 */

export const ACTION_TYPES = {
  FETCH_REQUEST: `${DOMAIN_NAME}/FETCH_REQUEST`,
  FETCH_CANCELLED: `${DOMAIN_NAME}/FETCH_CANCELLED`,
  FETCH_SUCCESS: `${DOMAIN_NAME}/FETCH_SUCCESS`,
  FETCH_FAILURE: `${DOMAIN_NAME}/FETCH_FAILURE`,
  UPDATE_STATUS: `${DOMAIN_NAME}/UPDATE_STATUS`,
};

/**
 * Action Creators
 */

export function fetchGenre() {
  return { type: ACTION_TYPES.FETCH_REQUEST }
}

export function cancelFetchGenre() {
  return { type: ACTION_TYPES.FETCH_CANCELLED }
}

export function fetchGenreSuccess(payload: ResponseSuccessType) {
  return {
    type: ACTION_TYPES.FETCH_SUCCESS,
    payload,
  }
}

export function fetchGenreFailure(payload: ResponseFailureType) {
  return {
    type: ACTION_TYPES.FETCH_FAILURE,
    payload,
  }
}

export function updateStatusGenre(payload: GenreType) {
  return {
    type: ACTION_TYPES.UPDATE_STATUS,
    payload
  }
}
