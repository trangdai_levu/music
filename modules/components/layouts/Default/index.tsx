import React from "react";
import styles from "./styles/Layout.module.scss";

type Props = {
  children: React.ReactNode,
}

export default function Layout({ children }: Props) {
  return (
    <div className={styles.layout}>
      {/* maybe include Header, Fooder, Sidebar... */}
      {children}
    </div>
  );
}
