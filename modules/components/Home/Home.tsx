import { useSelector } from "react-redux";
import NoData from "core/components/NoData";
import Card from "core/components/Card";
import { MusicType } from "core/types/music.type";
import { selectMusics, selectResultCount } from "modules/slice";
import Genres from "../Genres";
import SearchBar from "./SearchBar";
import styles from "./styles/Home.module.scss";

export default function Home() {
  const resultCount: number = useSelector(selectResultCount);
  const musics: (MusicType[] | null) = useSelector(selectMusics);

  return (
    <div className={styles.page}>
      <SearchBar />
      <main className={styles.main}>
        <div className="container">
          <h2 className={styles.title}>Filter Genre</h2>
          <Genres />
        </div>

        <div className="container">
          <h2 className={styles.title}>Results ({resultCount})</h2>
          {(!resultCount || !musics)
            ? <NoData />
            : <div className={styles.musicList}>
              {musics.map((musicTrack, index) => (
                <Card
                  key={index}
                  title={musicTrack.trackName}
                  description={musicTrack.artistName}
                  imageUrl={musicTrack.artworkUrl100}
                />
              ))}
            </div>
          }
        </div>
      </main>
    </div>
  )
}
