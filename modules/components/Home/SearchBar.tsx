import React, { useState, useCallback } from "react";
import debounce from "lodash.debounce";
import { useSelector, useDispatch } from "react-redux";
import SearchIcon from "assets/icons/Search";
import { GenreType } from "core/types/genre.type";
import styles from "./styles/SearchBar.module.scss";
import {
  selectSearchText,
  selectGenres,
  fetchMusic,
  updateSearchText,
} from "modules/slice";

export default function SearchBar() {
  const searchText  = useSelector(selectSearchText);
  const genres: (GenreType[] | null) = useSelector(selectGenres);
  const [searchTerm, setSearchTerm] = useState(searchText);
  const dispatch = useDispatch();

  const debouncedSearch = useCallback(
    debounce((searchVal: string) => {
      const selectedGenres = genres?.filter((genre: GenreType) => genre.selected);
      const genresText = selectedGenres?.reduce((acc, genre: GenreType) => acc + genre.name + " ", "");
      dispatch(updateSearchText(searchVal));
      dispatch(fetchMusic({
        ... {term: `${searchVal} ${genresText}`},
        ...selectedGenres?.length && {attribute: "genreIndex"}
      }));
    }, 1000),
    []
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
    debouncedSearch(event.target.value);
  };

  return (
    <div className={styles.wrapper}>
      <div className="container">
        <div className={styles.searchBox}>
          <input
            type="text"
            className={styles.input}
            placeholder="Search your entertainment "
            value={searchTerm}
            onChange={handleChange}
          />
          <SearchIcon className={styles.icon} />
        </div>
      </div>
    </div>
  )
}
