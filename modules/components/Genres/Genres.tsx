import clsx from "clsx";
import { useSelector, useDispatch } from "react-redux";
import { GenreType } from "core/types/genre.type";
import Tag from "core/components/Tag";
import {
  selectSearchText,
  selectGenres,
  selectNextSelectedGenres,
  updateStatusGenre,
  fetchMusic,
} from "modules/slice";
import styles from "./styles/Genres.module.scss";

export default function Genres({ ...restProps }) {
  const genres: (GenreType[] | null) = useSelector(selectGenres);
  const searchText  = useSelector(selectSearchText);
  const dispatch = useDispatch();

  if (!genres) return null;

  const onClick = (val: GenreType) => {
    const updatedGenre = {...val, selected: !val.selected};
    const selectedGenres = selectNextSelectedGenres(genres, updatedGenre);
    const genresText = selectedGenres?.reduce((acc, genre: GenreType) => acc + genre.name + " ", "");
    dispatch(updateStatusGenre(updatedGenre));
    dispatch(fetchMusic({
      ... {term: `${searchText} ${genresText}`},
      ...selectedGenres.length && {attribute: "genreIndex"}
    }));
  };

  return (
    <div className={styles.genres} {...restProps}>
      {genres.map((tag: GenreType, index: number) => (
        <Tag
          key={index}
          name={tag.name}
          onClick={() => onClick(tag)}
          className={clsx(styles.genre, tag.selected && "selected")}
        />
      ))}
    </div>
  )
}
