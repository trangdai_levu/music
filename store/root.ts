import { combineReducers } from "redux";
import { all } from "redux-saga/effects";
import {
  GENRE_SLICE,
  GenreStateType,
  genreSaga,
  genreReducer,
  MUSIC_SLICE,
  MusicStateType,
  musicSaga,
  musicReducer,
} from "modules/slice";


export type StateType = {
  [GENRE_SLICE]: GenreStateType,
  [MUSIC_SLICE]: MusicStateType,
}

export const rootReducer = combineReducers({
  [GENRE_SLICE]: genreReducer,
  [MUSIC_SLICE]: musicReducer,
});

export function* rootSaga() {
  yield all([
    genreSaga(),
    musicSaga(),
  ])
}
