const API_URL = `${Cypress.env("apiUrl")}`;

describe("API ❖ Genre test", function () {

  context("GET /api/genre", function () {
    it("gets a list of genre", function () {
      cy.request("GET", `${API_URL}/api/genre`).then((response) => {
        expect(response.status).to.eq(200);
        expect(response.status).to.eq(200);
        expect(response).to.have.property("headers");
        expect(response).to.have.property("duration");
        expect(response.body.results).to.have.length(15);
        expect(response.body.resultCount).to.eq(15);
      });
    });
  });
});
