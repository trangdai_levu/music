// enables intelligent code completion for Cypress commands
// https://on.cypress.io/intelligent-code-completion
/// <reference types="Cypress" />

context("Page ★ Index", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should find the title of the homepage", () => {
    cy.get("h2").contains("Filter Genre");
    cy.get("input").invoke("attr", "placeholder")
      .should("contain", "Search your entertainment ");
  });
});
