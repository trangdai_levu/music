# Music App

---


## 📌 Notice:

- I can't get the font **Avenir Next** in design, so I use font **Mulish** instead.
- I can't get genre from iTunes, do I create a temporary api and handle feature "Filter by genre" a bit difference.



## Getting Started

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

##### Or see it on [vercel](https://music-phi-umber.vercel.app/)



## Project Structure

```
├── assets                 # contains images, icons, global styles...
│   ├── images
│   ├── icons
│   ├── styles
│   └── ...
├── core                   # utility functions, constants, basic presentation components...
│   ├── utils
│   ├── helpers
│   ├── types
│   ├── components
│   └── ...
├── store                  # configure store, gathering reducers and sagas.
├── modules
│   ├── slice              # each slice is a collection of reducer logic and actions... for a single feature.
│   ├── components         # contains related pieces of code (components, state, business logic, ...) grouped together.
├── pages                  # reflects the routes of the application.

```

> Nextjs provides some folders like: **pages**, **public**, eslint, configs... We don't explain it above ↑



## Framework and libraries used in this project

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

- 🎨 Styling: SCSS, CSS modules, clsx.
- 💾 State Management: Redux, Redux Saga, reselect, immer and supported by next-redux.

